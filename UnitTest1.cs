﻿using System;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace sample_Git
{


	[TestClass]
	public class UnitTest1
	{
		[TestMethod]
		public void TestMethod1()
		{

			int a = 1;  //a是var<int>
			int b = a;      //b是var<int>
			a++;    //a中的int值改变了。

			///然后单步执行 (右键点击，选择菜单"Debug Test|调试 测试"里的单步执行)代码，观察a,b的变化（把鼠标悬停在a和b的上方，分别查看）。
			///体会：两个 值类型 变量 直接 存放了 数据的值，而非地址。

			StringBuilder builder = new StringBuilder();
			StringBuilder builder1 = builder;   //builder1是Var<StringBuilder>。
			builder.Append("a");    ///builder这里代表了StringBuilder的指代。
			///单步执行观察builder和builder1的变化。体会：两个引用类型变量存放了同一个数据实例的地址，因此指向同一个数据


			builder = null;
			builder1.Append("b");
			builder.Append("b");    ///builder不代表StringBuilder值，而是Var<StringBuilder>值null，所以不能进行StringBuilder实例的操作。
			///体会： builder存放了空地址，不指向任何数据，而 ".Append" 只能用于运算数据实例；这样，发生了异常。



		}
	}
}

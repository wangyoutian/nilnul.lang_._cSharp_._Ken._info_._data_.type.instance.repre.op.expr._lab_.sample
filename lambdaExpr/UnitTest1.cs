﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace sample_Git.lambdaExpr
{
	[TestClass]
	public class UnitTest1
	{
		[TestMethod]
		public void TestMethod1()
		{
			//右击，选择 “调试单元测试”
			Func<int, int, int> a = (int x, int y) => x + y;

			var b = a(3, 5);		//停在此处，暂停，把鼠标悬停在b上方（或者查看VS下方的变量显示面板）查看b的值。

			///按F10(或者在上方工具栏里点“单步执行”) 完成 上一步（暂停在下一步前），再次查看b的值。
		}
	}
}
